function supportpkg = stellaris_lp_info()
%STELLARIS_LP_INFO Return  information.

%   Copyright 2013 The MathWorks, Inc.

supportpkg = hwconnectinstaller.SupportPackage();
supportpkg.Name          = 'Stellaris LP';
supportpkg.Version       = '99.99';
supportpkg.Platform      = 'PCWIN,PCWIN64';
supportpkg.Visible       = '1';
supportpkg.FwUpdate      = '';
supportpkg.Url           = 'http://bit.ly/run_on_stellaris_lp';
supportpkg.DownloadUrl   = 'http://bit.ly/run_on_stellaris_download';
supportpkg.LicenseUrl    = 'http://creativecommons.org/licenses/by-nc-sa/3.0/';
supportpkg.BaseProduct   = 'Simulink';
supportpkg.FullName      = '';
supportpkg.DisplayName      = 'Stellaris LP';
supportpkg.CustomLicense = '';
supportpkg.CustomLicenseNotes = '';
supportpkg.ShowSPLicense = true;
supportpkg.Folder        = 'StellarisLP';
supportpkg.Release       = '(R2013a)';
supportpkg.DownloadDir   = 'C:\peng\Libs\Matlab\Targets\deployment';
supportpkg.InstallDir    = 'C:\MATLAB\SupportPackages\R2013a';
supportpkg.IsDownloaded  = 0;
supportpkg.IsInstalled   = 1;
supportpkg.RootDir       = 'C:\MATLAB\SupportPackages\R2013a\StellarisLP';
supportpkg.DemoXml       = 'demo/demos.xml';
supportpkg.FwUpdate      = '';
supportpkg.TargetHardware(1).Name     = 'Stellaris LP';
supportpkg.TargetHardware(1).DataFile = 'StellarisLP';
supportpkg.TargetHardware(2).Name     = 'Stellaris LP1';
supportpkg.TargetHardware(2).DataFile = 'StellarisLP';
supportpkg.Path{1}      = 'C:\MATLAB\SupportPackages\R2013a\StellarisLP';
supportpkg.Path{2}      = 'C:\MATLAB\SupportPackages\R2013a\StellarisLP\blocks';
supportpkg.Path{3}      = 'C:\MATLAB\SupportPackages\R2013a\StellarisLP\blocks\mex';

% Third party software information
supportpkg.TpPkg(1) = hwconnectinstaller.ThirdPartyPackage('No TP Pkg', '');
supportpkg.TpPkg(1).Url        = '';
supportpkg.TpPkg(1).DownloadUrl = 'http://creativecommons.org/licenses/by-nc-sa/3.0/';
supportpkg.TpPkg(1).LicenseUrl = '';
supportpkg.TpPkg(1).FileName = '';
supportpkg.TpPkg(1).DestDir = 'StellarisLP';
supportpkg.TpPkg(1).Installer = '';
supportpkg.TpPkg(1).Archive = '';
supportpkg.TpPkg(1).DownloadDir = 'C:\peng\Libs\Matlab\Targets\deployment';
supportpkg.TpPkg(1).InstallDir = 'C:\MATLAB\SupportPackages\R2013a';
supportpkg.TpPkg(1).IsDownloaded = 0;
supportpkg.TpPkg(1).IsInstalled = 1;
supportpkg.TpPkg(1).InstallCmd = '';
supportpkg.TpPkg(1).RemoveCmd = '';
supportpkg.TpPkg(1).RootDir = 'C:\MATLAB\SupportPackages\R2013a\StellarisLP';
