/*
 * uart_driver.h
 *
 *  Created on: Sep 30, 2013
 *      Author: Sippey
 */

#ifndef UART_DRIVER_MP_H_
#define UART_DRIVER_MP_H_

void UART0IntHandler(void);

// low level functions for early boot time debugging
void uart_init_hw(unsigned int channel, unsigned long baud, int interrupt_mode);
void uart_bare_init(void);
void uart_bare_putchar(char c);

// normal function set
void uart_init(void);

int uart_port_read_pending(unsigned int port);
int uart_port_write_pending(unsigned int port);
int uart_port_read_one(unsigned int port, unsigned char* buf);
int uart_port_write_one(unsigned int port, unsigned char* buf);
int uart_port_open( unsigned int port,
					unsigned char* in_buf, unsigned int in_buf_size,
					unsigned char* out_buf, unsigned int out_buf_size);
int uart_port_close(unsigned int port);

///////////// UART 0 DEFINES /////////////////////
#define UART_BAUD_RATE	 		1000000//921600
#define UART_INTERRUPT_MODE     1
#define UART_POLL_MODE          0


#define UART_N_PORTS			10
#define UART_CMD_PORT			0xF0
#define UART_CMD_SYNC			0xFA
#define UART_CMD_ESC			0xFF

#define UART_STATUS_UNINITED    0x00
#define UART_STATUS_INITED    	0xFA
#define UART_STATUS_PORT_MASK	0x0F
#define UART_STATUS_ESC_MASK	0xEF

#define PORT_STATUS_UNINITED    0x00
#define PORT_STATUS_INITED    	0xFA


#endif /* UART_DRIVER_MP_H_ */
