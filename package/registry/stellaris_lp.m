% NOTE: DO NOT REMOVE THIS LINE XMAKEFILE_TOOL_CHAIN_CONFIGURATION
function toolChainConfiguration = stellaris_lp()
%STELLARIS_LP Defines a tool chain configuration

ArmCC = realtime.internal.getStellarisLPInfo('ArmCCDir');
TargetRoot = realtime.internal.getStellarisLPInfo('PackageDir');


% General
toolChainConfiguration.Configuration = 'stellaris_lp';
toolChainConfiguration.Version = '1.0';
toolChainConfiguration.Description = 'Stellaris Launchpad';
toolChainConfiguration.Operational = true;
%toolChainConfiguration.InstallPath = 'C:\Program Files (x86)\Microsoft Visual Studio 8\';
toolChainConfiguration.Decorator = 'linkfoundation.xmakefile.decorator.eclipseDecorator';
% Make
toolChainConfiguration.MakePath = @(src) 'gmake'; %(tiCCSDir, 'utils', 'bin', 'gmake');
% toolChainConfiguration.MakePath = 'C:\Arduino22\hardware\tools\avr\utils\bin\make';
toolChainConfiguration.MakeFlags = '-f "[|||MW_XMK_GENERATED_FILE_NAME[R]|||]" [|||MW_XMK_ACTIVE_BUILD_ACTION_REF|||]';
toolChainConfiguration.MakeInclude = '';
% Compiler
toolChainConfiguration.CompilerPath = @(src) fullfile(ArmCC, 'bin', 'armcl');
toolChainConfiguration.CompilerFlags = '-c ';%-mv7M4 --code_state=16 --abi=eabi -me -O2 --gcc';
toolChainConfiguration.SourceExtensions = '.c,.cpp,.asm';
toolChainConfiguration.HeaderExtensions = '.h';
toolChainConfiguration.ObjectExtension = '.obj';
% Linker
toolChainConfiguration.LinkerPath = @(src) fullfile(ArmCC, 'bin', 'armlnk');
toolChainConfiguration.LinkerFlags = '-o [|||MW_XMK_GENERATED_TARGET_REF|||]';
toolChainConfiguration.LibraryExtensions = '.lib,.a';
toolChainConfiguration.TargetExtension = '.out';
toolChainConfiguration.TargetNamePrefix = '';
toolChainConfiguration.TargetNamePostfix = '';
% Archiver
toolChainConfiguration.ArchiverPath = @(src) fullfile(ArmCC, 'bin', 'armar');
toolChainConfiguration.ArchiverFlags = '/OUT:[|||MW_XMK_GENERATED_TARGET_REF|||]';
toolChainConfiguration.ArchiveExtension = '.a';
toolChainConfiguration.ArchiveNamePrefix = 'lib';
toolChainConfiguration.ArchiveNamePostfix = '';
% Pre-build
toolChainConfiguration.PrebuildEnable = false;
toolChainConfiguration.PrebuildToolPath = '';
toolChainConfiguration.PrebuildFlags = '';
% Post-build
% hex     

if ispc
    % obj2bin tools 
    ofd = fullfile(ArmCC,'bin','armofd');
    hex = fullfile(ArmCC,'bin','armhex');
    mkhex = fullfile(TargetRoot,'utils','tiobj2bin','mkhex4bin');
    tiobj2bin = fullfile(TargetRoot,'utils','tiobj2bin','tiobj2bin.bat');
    %system(['"' tiobj2bin '" "' outfile '" "' binfile  '" ]);
    
    toolChainConfiguration.PostbuildEnable = true;
    toolChainConfiguration.PostbuildToolPath = tiobj2bin; 
    toolChainConfiguration.PostbuildFlags = [ '$(TARGET) $(OUTPUT_PATH)$(MODEL_NAME).bin "' ofd '" "' hex '" "' mkhex '"'];
else
    toolChainConfiguration.PostbuildEnable = false;
    toolChainConfiguration.PostbuildToolPath = ''; 
    toolChainConfiguration.PostbuildFlags = '';
end

% Execute
toolChainConfiguration.ExecuteDefault = false;
toolChainConfiguration.ExecuteToolPath = '';
toolChainConfiguration.ExecuteFlags = '';

% Directories
toolChainConfiguration.DerivedPath = '[|||MW_XMK_SOURCE_PATH_REF|||]';
toolChainConfiguration.OutputPath = '';
% Custom
toolChainConfiguration.Custom1 = '';
toolChainConfiguration.Custom2 = '';
toolChainConfiguration.Custom3 = '';
toolChainConfiguration.Custom4 = '';
toolChainConfiguration.Custom5 = '';
end
