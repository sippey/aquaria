/*
 * mpu6050.c
 *
 *  Created on: Nov 11, 2013
 *      Author: Sippey
 */


#ifdef MATLAB_MEX_FILE
// build mex file
    // no code in this one.
#else

#include "stellaris_runtime.h"
#include "mpu6050.h"

#define MPU6050_ADDR    MPU6050_DEFAULT_ADDRESS

void MPU6050Init(void)
{
    // reset
	i2c_writebit(MPU6050_ADDR, MPU6050_RA_PWR_MGMT_1, MPU6050_PWR1_DEVICE_RESET_BIT, 1);
    system_delay_ms(100);
    
    // set gyro range
	i2c_writebits(MPU6050_ADDR, 
                  MPU6050_RA_GYRO_CONFIG, 
                  MPU6050_GCONFIG_FS_SEL_BIT, 
                  MPU6050_GCONFIG_FS_SEL_LENGTH, 
                  MPU6050_GYRO_FS_500);
    // set acc range
	i2c_writebits(MPU6050_ADDR, 
                  MPU6050_RA_ACCEL_CONFIG, 
                  MPU6050_ACONFIG_AFS_SEL_BIT, 
                  MPU6050_ACONFIG_AFS_SEL_LENGTH, 
                  MPU6050_ACCEL_FS_4);
    
    // out of sleep
	i2c_writebit(MPU6050_ADDR, 
                 MPU6050_RA_PWR_MGMT_1, 
                 MPU6050_PWR1_SLEEP_BIT, 0);
}

int MPU6050ReadRaw(short acc[3], short gyro[3], short* temp)
{
	unsigned char buffer[14];
    int ret;

    ret = i2c_readbytes(MPU6050_ADDR, MPU6050_RA_ACCEL_XOUT_H, 14, buffer);
    
    if (ret)
    {
        *acc =   (((short)buffer[0]) << 8)  | buffer[1];
        *(acc+1) =   (((short)buffer[2]) << 8)  | buffer[3];
        *(acc+2) =   (((short)buffer[4]) << 8)  | buffer[5];

        *temp = (((short)buffer[6]) << 8)  | buffer[7];
        *temp = *temp*100/340+3653; // celcius degree x 100 

        *(gyro)=   (((short)buffer[8]) << 8)  | buffer[9];
        *(gyro+1) =   (((short)buffer[10]) << 8) | buffer[11];
        *(gyro+2) =   (((short)buffer[12]) << 8) | buffer[13];
        return 1;
    }else
    {
        return 0;
    }
}

#endif
