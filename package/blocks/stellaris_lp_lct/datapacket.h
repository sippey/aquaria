#ifndef DATAPACKET_H_
#define DATAPACKET_H_

#ifdef MATLAB_MEX_FILE

    #define init_datapacket(a,b,c)      (0)
    #define datapacket_get(a,b,c)          (0)
    #define datapacket_put(a,b,c)          (0)
#else

int init_datapacket(int port, int in_pkt_size, int out_pkt_size);
int datapacket_get(int port, int in_pkt_size, unsigned char *data);
int datapacket_put(int port, int in_pkt_size, unsigned char *data);

#if 0
typedef struct 
{
    unsigned char* in_buf;
    unsigned char* out_buf;
    unsigned int port;
    unsigned int pkt_size;
    int input_synced;
}datapacket_reg, * p_datapacket_reg;

#define SYNC_THRESHOLD 3
#define SYNC_CHAR 0xAA

int init_datapacket(int pkt_size, int port, p_datapacket_reg p);
int datapacket_get(p_datapacket_reg p, unsigned char * data);
#endif



#endif //MATLAB_MEX_FILE

#endif /* DATAPACKET_H_ */
