/*
 * ls7366r.c
 *
 *  Created on: Nov 11, 2013
 *      Author: Sippey
 */



#ifdef MATLAB_MEX_FILE
// build mex file

    // no code in this one.

#else
// build realtime target

#include "stellaris_runtime.h"
#include "ssi_API.h"
#include "ls7366r.h"

// STR. The STR is an 8-bit status register which stores
// count related status information.
#define CY      1 << 7  //Carry (CNTR overflow) latch
#define BW      1 << 6  //Borrow (CNTR underflow) latch
#define CMP     1 << 5  //Compare (CNTR = DTR) latch
#define IDX     1 << 4  //Index latch
#define CEN 1 << 3      //Count enable status: 0: counting disabled,
                                                                //1: counting enabled
#define PLS     1 << 2  //Power loss indicator latch; set upon power up
#define U_D     1 << 1  //Count direction indicator: 0: count down, 1: count up
#define S       1       //Sign bit. 1:

// IR. The IR is an 8-bit register that fetches instruction bytes from
// the received data stream and executes them to perform such
// functions as setting up the operating mode for the chip (load the
// MDR) and data transfer among the various registers.
 //B2 B1 B0 = XXX (Don�t care)
 //B5 B4 B3
#define SELECT_none     0 << 3
#define SELECT_MDR0     1 << 3
#define SELECT_MDR1     2 << 3
#define SELECT_DTR      3 << 3
#define SELECT_CNTR     4 << 3
#define SELECT_OTR      5 << 3
#define SELECT_STR      6 << 3
#define SELECT_none_2   7 << 3
 //B7 B6 = 00:
#define CLR_register    0 << 6
#define RD_register     1 << 6
#define WR_register     2 << 6
#define LOAD_register   3 << 6

// MDR0. The MDR0 (Mode Register 0) is an 8-bit read/write register that sets up the operating mode
// for the LS7366R. The MDR0 is written into by executing the "write-to-MDR0" instruction via the
// instruction register. Upon power up MDR0 is cleared to zero.
#define non_quadrature_count_mode 0
#define x1_quadrature_count_mode 1
#define x2_quadrature_count_mode 2
#define x4_quadrature_count_mode 3

#define free_running_count_mode 0 <<2
#define single_cycle_count_mode 1 <<2
#define range_limit_count_mode 2<<2
#define modulo_n_count_mode 3<<2

#define disable_index 0<<4
#define configure_index_load_CNTR 1<<4
#define configure_index_reset_CNTR 2<<4
#define configure_index_load_OTR 3<<4

#define Asynchronous_Index 0<<6
#define Synchronous_Index 1<<6

#define Filter_clock_division_factor_1 0<<7
#define Filter_clock_division_factor_2 1<<7


#define MDR1_4_byte_counter_mode 0
#define MDR1_3_byte_counter_mode 1
#define MDR1_2_byte_counter_mode 2
#define MDR1_1_byte_counter_mode 3

#define Enable_Counting 0<<2
#define Disable_Counting 1<<2



void ls7366r_clear(int ssel, unsigned char reg);
void ls7366r_write(int ssel, unsigned char reg, unsigned char data);
void ls7366r_read(int ssel, unsigned char reg, unsigned char * pdata);

void init_ls7366r(int ssel, int clk)
{
    static int pd6_inited=0;
    if (!pd6_inited && clk)
    {
        pwm_set_pd6_clk(); //pwm PD6 as clk
        pd6_inited = 1;
    }
	ls7366r_clear(ssel, SELECT_CNTR);
	ls7366r_write(ssel, SELECT_MDR0, x1_quadrature_count_mode);
}

void ls7366r_clear(int ssel, unsigned char reg)
{
	unsigned char cmd = 0;

	cmd = reg | CLR_register;

	ssi0_ss(0);
	if (ssi0_ss2(ssel))
    {
        ssi0_write(cmd);
        ssi0_ss(0);
    }else
    {
        //nothing
    }
}

void ls7366r_write(int ssel, unsigned char reg, unsigned char data)
{
	unsigned char cmd = 0;

	cmd = reg | WR_register;

	ssi0_ss2(0);
	if (ssi0_ss2(ssel))
    {
        ssi0_write(cmd);
        ssi0_write(data);

        ssi0_ss2(0);
    }else
    {
        //nothing
    }
}

void ls7366r_read(int ssel, unsigned char reg, unsigned char * pdata)
{
	unsigned char cmd = 0;
	unsigned long buf;

	cmd = reg | RD_register;

	ssi0_ss2(0);
	if (ssi0_ss2(ssel))
    {
        ssi0_write(cmd);
        ssi0_read(&buf);
        *pdata =  buf & 0xff;
        ssi0_ss(0);
    }else
    {
        *pdata =0;
    }
}

unsigned int ls7366r_get_encoder(int ssel)
{
	unsigned char cmd;
	unsigned long buf;
	unsigned int  result=0;

	cmd = SELECT_CNTR | RD_register ;

	ssi0_ss2(0);
    if (ssi0_ss2(ssel))
    {
        ssi0_write(cmd);

        ssi0_read(&buf);
        result =  buf & 0xff;
        ssi0_read(&buf);
        result = (result <<8) |  (buf & 0xff);
        ssi0_read(&buf);
        result = (result <<8) |  (buf & 0xff);
        ssi0_read(&buf);
        result = (result <<8) |  (buf & 0xff);

        ssi0_ss(0);
    }else
    {
        result = 0;
    }

	return result;
}

#endif
