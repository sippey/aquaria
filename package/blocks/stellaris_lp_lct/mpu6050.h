#ifndef MPU6050_H_
#define MPU6050_H_

#ifdef MATLAB_MEX_FILE
// build mex file

    // no code in this one.
    #define MPU6050Init()  (void)(0)
    #define MPU6050ReadRaw(acc, gyro, temp)  (0)
#else


#include "MPU6050def.h"

void MPU6050Init(void);
int  MPU6050ReadRaw(short acc[3], short gyro[3], short* temp);

#define MPU6050_DEFAULT_ADDRESS     MPU6050_ADDRESS_AD0_LOW

#endif // MATLAB_MEX_FILE

#endif /* MPU6050_H_ */


