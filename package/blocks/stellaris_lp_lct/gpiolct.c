#include "gpiolct.h"

void GPIOWrite(unsigned long ulPort, unsigned char ucPins, unsigned char ucValue)
{
#ifndef MATLAB_MEX_FILE
    ROM_GPIOPinWrite(ulPort, ucPins, ucValue);
#endif
}

long GPIORead(unsigned long ulPort, unsigned char ucPins)
{
#ifndef MATLAB_MEX_FILE
    long lValue = 0;
    lValue = ROM_GPIOPinRead(ulPort, ucPins);
    return lValue;
#endif
}

void GPIOSetup(unsigned long ulPeripheral, unsigned long ulPort, unsigned char ucPinsOutput, unsigned char ucPinsInput)
{
#ifndef MATLAB_MEX_FILE
    ROM_SysCtlPeripheralEnable(ulPeripheral);
    ROM_GPIOPinTypeGPIOOutput(ulPort, ucPinsOutput);  // separated output and input
    ROM_GPIOPinTypeGPIOInput(ulPort, ucPinsInput);
#endif
}

void GPIOVoidFunc()
{

}
