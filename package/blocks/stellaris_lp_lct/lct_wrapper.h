#ifndef __LCT_WRAPPER_H
#define __LCT_WRAPPER_H

#ifdef MATLAB_MEX_FILE
    //  MEX FILE COMPILING
    #define ADCRead(ch_id) (0)
    #define PWMInit(ch_id, freq, inverted) (void(0))
    #define PWMStop(ch_id) (void(0))
    #define PWMUpdate(ch_id, percentX) (void(0))
    #define PWMUpdate_f(ch_id, percent) (void(0))

    #define DebugPrint(fmt, a, b, c, d)  (void(0))
    #define DebugPrint2(fmt,va) (void(0))
#else
    #include "stellaris_runtime.h"
    #include "xprintf.h"

    #define ADCRead         adc_get_channel

    #define PWMInit(ch_id, freq, inverted)  pwm_init_channel(ch_id, freq, inverted,  PWM_CTRL_ENABLE)
    #define PWMStop(ch_id)  pwm_init_channel(ch_id, 0, 0,  PWM_CTRL_DISABLE)   
    #define PWMUpdate       pwm_set_channel
    #define PWMUpdate_f     pwm_set_channel_f
    #define DebugPrint          xprintf
    #define DebugPrint2(fmt,va)	xprintf_v((const char*)(fmt), (va_list){(va)} )
#endif

#endif
