function def = lctdef_MPU6050Raw()
MPU6050Raw = legacy_code('initialize');
MPU6050Raw.SFunctionName = 'stellaris_lp_sfunc_MPU6050Raw';
MPU6050Raw.HeaderFiles = {'mpu6050.h'};
MPU6050Raw.SourceFiles =   {'mpu6050.c'};
MPU6050Raw.StartFcnSpec = 'MPU6050Init()';
MPU6050Raw.OutputFcnSpec = 'int32 y4 = MPU6050ReadRaw(int16 y1[3], int16 y2[3], int16 y3[1])';
MPU6050Raw.SampleTime = 'parameterized';

def = MPU6050Raw(:);
end