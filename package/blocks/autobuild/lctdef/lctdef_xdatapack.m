function def = lctdef_xdatapack()
%% XDataInit
XDataInit = legacy_code('initialize');
XDataInit.SFunctionName = 'stellaris_lp_sfunc_XDataInit';
XDataInit.HeaderFiles = {'xdatapacket.h'};
XDataInit.SourceFiles = {'xdatapacket.c'};
XDataInit.StartFcnSpec = 'init_xdatapacket(int32 p1)'; 
XDataInit.OutputFcnSpec = 'init_xdatapacket_void()'; % cannot ignore output fcn

%% XDataPackin
XDataPackin = legacy_code('initialize');
XDataPackin.SFunctionName = 'stellaris_lp_sfunc_XDataPackin';
XDataPackin.HeaderFiles = {'xdatapacket.h'};
XDataPackin.SourceFiles = {'xdatapacket.c'};
XDataPackin.StartFcnSpec = ''; 
XDataPackin.OutputFcnSpec = 'int32 y2 = xdatapacket_get(int32 p1, uint8 y1[p1])';
XDataPackin.SampleTime = 'parameterized';

%% XDataPackout
XDataPackout = legacy_code('initialize');
XDataPackout.SFunctionName = 'stellaris_lp_sfunc_XDataPackout';
XDataPackout.HeaderFiles = {'xdatapacket.h'};
XDataPackout.SourceFiles = {'xdatapacket.c'};
XDataPackout.StartFcnSpec = ''; 
XDataPackout.OutputFcnSpec = 'int32 y1 = xdatapacket_put(int32 p1, uint8 u1[p1])';
XDataPackout.SampleTime = 'parameterized';

def = [XDataInit; XDataPackin(:); XDataPackout(:)];
end