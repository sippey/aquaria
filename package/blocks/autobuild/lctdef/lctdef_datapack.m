function def = lctdef_datapack()
%% DataPackin
DataPackin = legacy_code('initialize');
DataPackin.SFunctionName = 'stellaris_lp_sfunc_DataPackin';
DataPackin.HeaderFiles = {'datapacket.h'};
DataPackin.SourceFiles = {'datapacket.c'};
DataPackin.StartFcnSpec = 'init_datapacket(int32 p1, int32 p2, int32 p3)';
DataPackin.OutputFcnSpec = 'int32 y2 = datapacket_get(int32 p1, int32 p2, uint8 y1[p2])';
DataPackin.SampleTime = 'parameterized';

DataPackout = legacy_code('initialize');
DataPackout.SFunctionName = 'stellaris_lp_sfunc_DataPackout';
DataPackout.HeaderFiles = {'datapacket.h'};
DataPackout.SourceFiles = {'datapacket.c'};
DataPackout.StartFcnSpec = 'init_datapacket(int32 p1, int32 p2, int32 p3)';
DataPackout.OutputFcnSpec = 'int32 y1 = datapacket_put(int32 p1, int32 p3, uint8 u1[p3])';
DataPackout.SampleTime = 'parameterized';

def =[DataPackin(:); DataPackout(:)];
end