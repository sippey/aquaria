function lctgen_cgir(sfunc_name)

%% load defs
lct_path = 'stellaris_lp_lct';
lctdef_path = 'lctdef';

cd(lctdef_path);
try
    lctdef_files = dir('lctdef_*.m');
    def = [];
    for i=1:length(lctdef_files)
        [~,name,~]=fileparts(lctdef_files(i).name);
        def = [def;feval(name)]; %#ok<*AGROW>
    end

    for i=1:length(def)
        def(i).Options.singleCPPMexFile = true;  % set cgir flag
        def(i).Options.useTlcWithAccel = false;
        def(i).IncPaths ={fullfile( '..', lct_path)};
        def(i).SrcPaths ={fullfile( '..', lct_path)};
    end
    cd('..');
catch 
    cd('..');
end
%%
if nargin>0
    if iscell(sfunc_name)
        sfunc_name_long = strcat('stellaris_lp_sfunc_',sfunc_name);
    else
        sfunc_name_long = {strcat('stellaris_lp_sfunc_',sfunc_name)};
    end
    def_compile=[];
    for i=1:length(def)
        if ~isempty(intersect(sfunc_name_long,def(i).SFunctionName))
            def_compile=[def_compile; def(i)];
            clear(def(i).SFunctionName); % try unload sfun in memory
        end
    end
else
    def_compile=def;
end

%% Legacy Code Tool
% Generate S-function C++ files for simulation and CGIR
legacy_code('SFCN_CMEX_GENERATE', def_compile);

% Compile C++ files under different platform
legacy_code('COMPILE', def_compile, {'-win32'});
legacy_code('COMPILE', def_compile, {'-win64'}); 

% Clean up intermediate cpp files
delete stellaris_lp_sfunc_*.cpp


%lct_path = 'stellaris_lp_lct';
%%
if  ~exist(fullfile('..','mex'),'dir')
    ret = mkdir(fullfile('..','mex'));
end
if  ~exist(fullfile('..',lct_path),'dir')
    ret = mkdir(fullfile('..',lct_path));
end
%ret = copyfile(fullfile('lct','*.c'), fullfile('..',lct_path),'f');
%ret = copyfile(fullfile('lct','*.h'), fullfile('..',lct_path),'f');
ret = movefile('*.mex*', fullfile('..', 'mex'));

%% Generate rtwmakecfg.m file to automatically set up some build options

for i=1:length(def)
    def(i).IncPaths ={lct_path};
    def(i).SrcPaths ={lct_path};
end
legacy_code('rtwmakecfg_generate', def);

fn = 'rtwmakecfg';
package = 'StellarisLP';

rtwmakecfg_txt = strrep(fileread([fn '.m']),'rtwmakecfg','rtwmakecfgStellarisLP');
rtwmakecfg_txt = [rtwmakecfg_txt ...
    'if (exist(''rtwmakecfgStellarisLP_user'',''file'') == 2), info = [info rtwmakecfgStellarisLP_user()]; end'];

rtwmakecfg_fp = fopen(fullfile('..', '..','+realtime','+internal',[fn package '.m']),'w');
fprintf(rtwmakecfg_fp, '%s',rtwmakecfg_txt);
fclose(rtwmakecfg_fp);
delete('rtwmakecfg.m');

end
