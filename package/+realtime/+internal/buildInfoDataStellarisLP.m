function info= buildInfoDataStellarisLP(~)
%buildInfoDataStellarisLP Defines build info data

target_define = 'TARGET_IS_BLIZZARD_RA1';

makeinfo = realtime.internal.rtwmakecfgStellarisLP;

info.SourceFiles            = i_getSourceFiles(makeinfo);
info.SourceFilesToSkip      = {'main.cpp'};
info.IncludePaths           = union(i_getIncludePaths, makeinfo.includePath);
info.CompileFlags           = [ '-mv7M4 '  ... arm7 cortex-m4
                                ...'--align_structs=4 ' ... align struct to workaround a compiler bug
                                '--code_state=16 '  ... 32 bit thumb code
                                '--float_support=FPv4SPD16 ' ... float support
                                '--abi=eabi '       ... ABI 
                                '-me '              ... little endian
                                '-O2 -mf4 '         ... optimalization lvl
                                ...'-O3 -mf4 '      ... optimalization lvl (will cause a problem that involves using LDMIA instruction on unaligned address)
                                '--gcc '             ... GCC extension
                                '-g ' ...
                                ...'--profile:breakpt ' ...
                                ];   
info.Defines                = { 'PART_LM4F120H5QR',       ...
                                target_define,            ...
                                'ccs=ccs',                ...
                                'UART_BUFFERED=1'         ... % to use buffered uart
                               } ;
info.LinkFlags              = [ '--stack_size=4096 '       ...
                                '--heap_size=4096 '          ...
                                '--reread_libs '          ...
                                '--rom_model "'           ...
                                    i_getLinkCmdFile      ...
                                    '" '                   ...
                                '--define=' target_define ' ' ...
                                '-i"' i_getLibPath '" ' ...
%                                '--args=128 ' ... % set space for cmd arg
                                ];
end

function link_cmd_file = i_getLinkCmdFile ()

pkgRootDir = realtime.internal.getStellarisLPInfo('PackageDir');
    link_cmd_file = fullfile(pkgRootDir, 'registry', 'stellaris_linkcmd');
end

% -------------------------------------------------------------------------
function files = i_getSourceFiles(makeinfo)
files = {};

%TICCSDir =  i_getTICCSDir();
packageDir =  realtime.internal.getStellarisLPInfo('PackageDir');

%add block sources
for i=1:length(makeinfo.sources)
    files{end+1}.Name = makeinfo.sources{i}; %#ok<*AGROW>
    files{end}.Path = i_getSourceFilePath(makeinfo.sourcePath, makeinfo.sources{i});
end

%{
cfiles = dir(fullfile(packageDir, 'blocks','*.c'));
for i=1:length(cfiles)
    files{end+1}.Name = cfiles(i).name; %#ok<*AGROW>
    files{end}.Path = fullfile(packageDir, 'blocks');
end
%}

files{end+1}.Name = 'reentrance.asm';
files{end}.Path = fullfile(packageDir, 'src');

files{end+1}.Name = 'startup_ccs.c';
files{end}.Path = fullfile(packageDir, 'src');

files{end+1}.Name = 'stellaris_runtime.c';
files{end}.Path = fullfile(packageDir, 'src');

files{end+1}.Name = 'rtiostreamserial_mp.c';
%files{end+1}.Name = 'rtiostreamserial.c';
files{end}.Path = fullfile(packageDir, 'src');

files{end+1}.Name = 'uart_driver_mp.c';
%files{end+1}.Name = 'uart_driver.c';
files{end}.Path = fullfile(packageDir, 'src');

files{end+1}.Name = 'ringbuf.c';
files{end}.Path = fullfile(packageDir, 'src');

files{end+1}.Name = 'ssi_API.c';
files{end}.Path = fullfile(packageDir, 'src');

files{end+1}.Name = 'I2C_Stellaris_API.c';
files{end}.Path = fullfile(packageDir, 'src');

%files{end+1}.Name = 'tfp_printf.c';
files{end+1}.Name = 'xprintf.c';
files{end}.Path = fullfile(packageDir, 'src');

files{end+1}.Name = 'uartx_driver.c';
files{end}.Path = fullfile(packageDir, 'src');

%files{end+1}.Name = 'mpu6050.c';
%files{end}.Path = fullfile(packageDir, 'src');

%files{end+1}.Name = 'ls7366r.c';
%files{end}.Path = fullfile(packageDir, 'src');

end

function p = i_getSourceFilePath(paths, file)
for i=1:length(paths)
    if exist(fullfile(paths{i},file),'file') == 2
        p=paths{i};
        return ;
    end
end
p=paths{1};
end

% -------------------------------------------------------------------------
function paths = i_getIncludePaths()
paths = {};
pkgRootDir = realtime.internal.getStellarisLPInfo('PackageDir');
armCCDir = realtime.internal.getStellarisLPInfo('ArmCCDir');
%paths{end+1} = fullfile(pkgRootDir, 'block');
paths{end+1} = fullfile(pkgRootDir, 'include');
paths{end+1} = fullfile(pkgRootDir, 'src');
paths{end+1} = fullfile(armCCDir, 'include');
paths{end+1} = realtime.internal.getStellarisLPInfo('StellarisWareDir');
end


function path = i_getLibPath()
armCCDir = realtime.internal.getStellarisLPInfo('ArmCCDir');
path = fullfile(armCCDir, 'lib');
end

