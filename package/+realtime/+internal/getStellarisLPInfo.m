function info = getStellarisLPInfo( name)
switch(name)
    case 'ctype_name'
        info = 'stellaris_lp';
    case 'CTypeName'
        info = 'StellarisLP';
    case 'Package Name'
        info = 'Stellaris LP';
    case 'COMPORT'
        info.COMPort    = getpref('stellaris_lp','COMPort'); %'COM11';
        info.COMPortNumber = num2str(sscanf(info.COMPort,'COM%d')); %'11';
        info.UploadRate = '115200';
    case 'PackageDir'
        hPkgInfo = realtime.internal.getStellarisLPInfo('PkgInfo');
        spPkg  = hPkgInfo.getSpPkgInfo('Stellaris LP');
        info = spPkg.RootDir;
    case 'CCSDir'
        info = getpref('stellaris_lp','CCSDir'); 
    case 'StellarisWareDir'
        info = getpref('stellaris_lp','StellarisWareDir');
    case 'ArmCCDir'
        compilerDir = realtime.internal.getStellarisLPInfo('ArmCCDir_User');
        if isempty(compilerDir)
%            tiCCSDir = realtime.internal.getStellarisLPInfo('CCSDir');
%             compilerDirs = dir(fullfile( tiCCSDir,'tools','compiler', 'arm_*'));
%             for i = 1:length(compilerDirs)
%                 if compilerDirs(i).isdir 
%                     compilerDir = ...
%                         fullfile( tiCCSDir,'tools','compiler', compilerDirs.name);
%                     break;
%                 end
%             end
            compilerDir = getpref('stellaris_lp', 'ArmCCDir');
        end
        info = compilerDir;
    case 'ArmCCDir_User'
        info = '';
        %info = 'P:\Software\ti\ccsv5\tools\compiler\arm_5.0.1';
    case 'BlockSrcIncInfo'
        info = realtime.internal.rtwmakecfgStellarisLP();
    case 'PkgInfo'
        matlab_ver = ver('matlab');
        ver_str = matlab_ver.Version;
        switch (ver_str)
            case {'8.0', '8.1', '8.2', '8.3', '8.4'} %(2012b) - (2014b)
                info = hwconnectinstaller.PackageInfo;
            case '7.14' %(2012a)
                info = realtime.setup.PackageInfo; 
            otherwise
                error('Matlab version not supported. Supported version includes 2012a/b 2013a');
        end
    otherwise 
        info = [];
end
end