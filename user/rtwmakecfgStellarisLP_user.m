function [ array ] = rtwmakecfgStellarisLP_user
%RTWMAKECFGSTELLARISLP_USER Summary of this function goes here
%   Detailed explanation goes here
info_tmpl = struct(...
    'SFunctionName', '',...
    'IncPaths', {{}},...
    'SrcPaths', {{}},...
    'LibPaths', {{}},...
    'SourceFiles', {{}},...
    'HostLibFiles', {{}},...
    'TargetLibFiles', {{}},...
    'singleCPPMexFile', false,...
    'Language', ''...
    );
array = [];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% paste block declaration below this block                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% stellaris_lp_sfunc_LED
info = info_tmpl;
info(1).IncPaths = {'C:\Peng\Work\Aquaria\user'};
info(1).Language = 'C';
info(1).SFunctionName = 'stellaris_lp_sfunc_LED';
info(1).SourceFiles = {'led.c'};
info(1).SrcPaths = {'C:\Peng\Work\Aquaria\user'};
info(1).singleCPPMexFile = 1;
array = [array info];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% paste block declaration above this block                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end

