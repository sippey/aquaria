/*
 * led.h
 *
 *  Created on: Jan 15, 2014
 *      Author: Sippey
 */

#ifndef LED_H_
#define LED_H_

#ifdef MATLAB_MEX_FILE

    #define led_output(a,b,c)  ((void)(0))

#else

    void led_output(int r, int g, int b);

#endif

#endif /* LED_H_ */
