// MP_Loopback_UDP.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "windows.h"
#include "xPCUDPSock.h"



#pragma pack(push,  1)   
struct  PACKIN{
		double sin_p;
		float sin_n;
		int sn;
};
struct  PACKOUT{
		float sin_p;
		double sin_n;
		int sn;
};
#pragma pack(pop)   


int _tmain(int argc, _TCHAR* argv[])
{
	if (!InitUDPLib())
	{
		printf("Init UDP Lib failed! ");
		return 1;
	}
	CUDPReceiver recver(sizeof(PACKIN), 12402);
	CUDPSender   sender(sizeof(PACKOUT), 12303, "127.0.0.1");

	PACKIN din;
	PACKOUT dout;
	int i=0;
	printf("running...\n");
	while(1)
	{
		recver.GetData(&din);

		dout.sin_p = float(din.sin_p);
		dout.sin_n = din.sin_n;
		dout.sn = din.sn;

		sender.SendData(&dout);
		Sleep(2);
		if ((i++ % 200) ==0)
		{
			//printf(".");
			//printf("%f %f %d\n", dout.sin_p, dout.sin_n, dout.sn);
			printf("sn = %d\n", dout.sn);
		}
	}

	return 0;
}

