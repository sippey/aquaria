#include "stdafx.h"
//#include <stdio.h>
#include <process.h>

#include "CComHubServer.h"
#include "CComPortEventHandler.h"
#include "Serial.h"


CComHubServer::CComHubServer(int comport, int baudrate):
		m_serial(NULL), m_rx_port(-1), m_tx_port(-1)
{
	char com_name[10];
	sprintf(com_name, "\\\\.\\COM%d",comport);
	m_serial = new Serial(com_name, baudrate);

	//m_serial->write("\xFA",1);
	
	memset(m_registra, 0, COMHUB_MAX_PORT*sizeof(CComPortEventHandler*));
	_beginthread(ComRxThread, 0, this);
}

void CComHubServer::ComRxThread(void * p)
{
	int len;
	char buffer;
	bool  escape = false;
	//bool  synced = true;

	CComHubServer* phub = (CComHubServer*)p;
	
	while(1)
	{
		len = phub->m_serial->read(&buffer, 1, false);

		if (len==-1)
		{
			printf("COM ERROR!\n");
			break;
		}else if (!len)
		{
			Sleep(1);
			continue;
		}

		//printf("Read Byte: %02X\n", (unsigned char)buffer);
		if (escape)
		{
			//printf("Got escaped char 0x%02X\n", (unsigned char) buffer);
			phub->SubmitToTcp(phub->m_rx_port, buffer);
			escape = false;
		}else if (buffer >= '\xF0' && buffer <= '\xF9')
		{
			phub->m_rx_port = buffer & 0x0F;
		}else if ((buffer & 0xF0) != 0xF0)
		{
			phub->SubmitToTcp(phub->m_rx_port, buffer);
		}else if (buffer == '\xFF')
		{
			//printf("Got Escaped Marker.. ");
			escape = true;
		}else if (buffer == '\xFA')
		{
			CAutoLockT<CLockableCS> lock(&(phub->m_tx_cs));
			phub->m_rx_port = -1;
			phub->m_tx_port = -1;
			escape = false;
			phub->m_serial->flush();
			phub->m_serial->write("\xFA", 1);
			phub->DisconnectTcpConnections();

			printf("Physical Com port synced!\n");
		}
	}
}

CComHubServer::~CComHubServer()
{
	if (m_serial)
	{
		delete m_serial;
	}
}

void CComHubServer::SubmitToTcp(int port, char data)
{
	if (port>=0 && port<=9 && m_registra[port])
	{
		m_registra[port]->OnComRecv(data);
	}
}

void CComHubServer::RegisterTcp(int port, CComPortEventHandler* p_tcp)
{
	m_registra[port] = p_tcp;
}

void CComHubServer::UnregisterTcp(int port)
{
	m_registra[port] = NULL;
}


void CComHubServer::DisconnectTcpConnections()
{
	int i;
	for (i=0;i<COMHUB_MAX_PORT;++i)
	{
		if (m_registra[i])
		{
			m_registra[i]->OnResetConnection();
		}
	}
}

void CComHubServer::Send(int port, char data)
{
	char ch;
	CAutoLockT<CLockableCS> lock(&m_tx_cs);
	if (port != m_tx_port)
	{
		ch = 0xF0 + port;
		m_serial->write(&ch, 1);
		m_tx_port = port;
	}
	if ((data & 0xF0) == 0xF0)
	{
		//need escape
		m_serial->write("\xFF", 1);
		//printf("Escape on char 0x%02X\n", (unsigned char) data);
	}
	m_serial ->write(&data,1);
}