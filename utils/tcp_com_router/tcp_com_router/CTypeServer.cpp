#include "stdafx.h"

#include <process.h>
#include "CTypeServer.h"

void CTypingServer::TypeServerThread(void *p)
{
	int len;
	PTYPING_SESSION ptyping_sess=(PTYPING_SESSION)p;
	while(!(ptyping_sess->p->m_terminate))
	{
		len = send(ptyping_sess->sock,"ABCDEFGHIJKLMNOPQRSTUVWXYZ\r\n",28,0);
		if (!len || len ==SOCKET_ERROR)
		{
			//socket error
			break;
		}
		Sleep(1000);
	}
	closesocket(ptyping_sess->sock);
	delete ptyping_sess;
}

void CTypingServer::OnConnected(SOCKET sock_in, sockaddr_in* si_in)
{
	//m_sess = sock_in;
	PTYPING_SESSION ptyping_sess= new TYPING_SESSION;
	ptyping_sess->p = this;
	ptyping_sess->sock = sock_in;

	_beginthread(TypeServerThread,0,ptyping_sess);
}
