#pragma once

#include "Serial.h"
#include "AutoLock.h"
#define COMHUB_MAX_PORT 10

class CComPortEventHandler;

using namespace AutoLock;

class CComHubServer
{
public:
	CComHubServer(int comport, int baudrate=256000);
	~CComHubServer();

	void RegisterTcp(int port, CComPortEventHandler* p_tcp);
	void UnregisterTcp(int port);
	void Send(int port, char data);

private:
	static void ComRxThread(void *p);
	void SubmitToTcp(int port, char data);
	void DisconnectTcpConnections();

	Serial* m_serial;
	CComPortEventHandler* m_registra[COMHUB_MAX_PORT];

	int					m_rx_port;
	int					m_tx_port;
	CLockableCS			m_tx_cs;
};
