#pragma once
#include <winsock.h>

class CTcpServer
{
public:
	CTcpServer(unsigned short port = 23001);
	CTcpServer(unsigned short port , unsigned long ipaddr);
	CTcpServer(unsigned short port , char * ipaddr_str);

	bool ConfigServer(unsigned short port, unsigned long ipaddr );
	~CTcpServer();
	int StartServer();
	void StopServer();
protected:
	virtual void OnConnected(SOCKET sock_in, sockaddr_in* si_in);
	bool   m_terminate;
private:
	static void ServerThread(void* p);
	sockaddr_in m_si;
	SOCKET m_sock;
	int    m_tid;

};