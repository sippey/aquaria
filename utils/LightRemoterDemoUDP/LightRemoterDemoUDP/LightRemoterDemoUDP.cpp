// LightRemoterDemoUDP.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


#include "windows.h"
#include "xPCUDPSock.h"

#pragma pack(push,  1)   

typedef struct
{
	int number1:16;
	unsigned pad1:1;
	unsigned r:1;
	unsigned b:1;
	unsigned g:1;
	unsigned pad2:4;
	int number2:8;
}DATA_PACK, *pDATA_PACK;

/*
// simpler struct definition
typedef struct
{
	short number1;
	char light;
	char number2;
}DATA_PACK, *pDATA_PACK;
*/
#pragma pack(pop)   


int _tmain(int argc, _TCHAR* argv[])
{
	char buffer[100];
	DATA_PACK data;

	if (!InitUDPLib())
	{
		printf("Cannot Initialize UDP library");
		getchar();
	}

	CUDPSender sender(sizeof(DATA_PACK), 12302, "127.0.0.1");
	
	printf("For light status, type \"r\" means red LED will light up, "
		   "and type \"rb\" means both red an blue will light up, type "
		   "\"off\" and all LED will be off.\n\n");

	while(1)
	{
		printf("Please input new light status: ");
		scanf("%s", buffer);
		
		memset(&data, 0, sizeof(DATA_PACK));

		data.number1 = rand() & 0xFFFF;
		data.number2 = rand() & 0xFF;

		if (strchr(buffer,'r') || strchr(buffer,'R')) data.r=1;
		if (strchr(buffer,'g') || strchr(buffer,'G')) data.g=1;
		if (strchr(buffer,'b') || strchr(buffer,'B')) data.b=1;

		//memset(&data, 0xff, sizeof(data));

		sender.SendData(&data);
	}
	return 0;
}

