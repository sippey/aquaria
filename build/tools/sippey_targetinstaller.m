function sippey_targetinstaller(varargin)
%%
% ex= hwconnectinstaller. Setup. get( ) ; 

if nargin == 0
    selected_mirror = get_repo_url('bbkt');
else
    if strcmp(varargin{1}, 'ver')
        disp('Last modified on 2/16/2015.');
        return;
    end
    selected_mirror = get_repo_url(varargin{1});
end


hwconnectinstaller.show(  );
hSetup = hwconnectinstaller.Setup.get(  );
ex = hSetup.Explorer;
%ex = hwconnectinstaller.show();
ex.hide;

root=ex.getRoot;
setup = root.getMCOSObjectReference;


setup.Installer.XmlHttp=selected_mirror;
%setup.CurrentStep.next('');
setup.Steps.Children(1).Children(3)=[]; % license page
setup.Steps.Children(1).Children(2)=[]; % login page
setup.CurrentStep.next('');
ex.title=[ex.title ' from Sippey''s repo'];
setup.Steps.Children(1).StepData.Labels.Internet='From Sippey''s Online Repo (Recommended)';
ex.show;
%ex.showTreeView(true);
end

function url = get_repo_url(name)
switch name
    case {'cu', '1'}
        url = 'http://people.clemson.edu/~pxu/rtt_repo';
    case {'sorg', '2'}
        url = 'http://web-service.sippey.org/matlab_repo';
    case {'sorgd', '3'}
        url = 'http://web-service.sippey.org/matlab_repo/debug';
    case {'bbkt', '4'}
        url = 'https://bitbucket.org/sippey/aquaria/wiki/matlab_repo';
end

end